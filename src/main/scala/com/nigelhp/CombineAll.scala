package com.nigelhp

object CombineAll {
  /*
   * We can use a fold to:
   * - sum a list of integers
   * - concatenate a list of strings
   * - join a list of lists
   * - union a list of sets
   *
   * Note that these all follow the same pattern.
   * There is some identity (or zero / empty) starting value, and an operation that combines 2 values.
   */
  def sumIntegers(values: List[Int]): Int =
    values.foldLeft(0)(_ + _)

  def concatenateStrings(values: List[String]): String =
    values.foldLeft("")(_ + _)

  def joinLists[A](values: List[List[A]]): List[A] =
    values.foldLeft(List.empty[A])(_ ++ _)

  def unionSets[A](values: List[Set[A]]): Set[A] =
    values.foldLeft(Set.empty[A])(_.union(_))


  /*
   * A Typeclass allows us to abstract this pattern, so that the three different functions above
   * (each dealing with a different type) can be replaced by a single function.
   *
   * The Typeclass is itself an abstract type, in this case defining that an implementation must
   * provide a definition of 'empty' and a combine operation for its associated type.
   */
  def combineAll[A](values: List[A], typeClass: Monoid[A]): A =
    values.foldLeft(typeClass.empty)(typeClass.combine)


  /*
   * The implementations that replicate the above behaviour are:
   */
  val intMonoid = new Monoid[Int] {
    override val empty = 0
    override def combine(x: Int, y: Int): Int = x + y
  }

  val stringMonoid = new Monoid[String] {
    override val empty = ""
    override def combine(x: String, y: String): String = x + y
  }

  def makeListMonoid[A]: Monoid[List[A]] = new Monoid[List[A]] {
    override val empty = List.empty[A]
    override def combine(x: List[A], y: List[A]): List[A] = x ++ y
  }

  def makeSetMonoid[A]: Monoid[Set[A]] = new Monoid[Set[A]] {
    override val empty = Set.empty[A]
    override def combine(x: Set[A], y: Set[A]): Set[A] = x.union(y)
  }

  /*
   * Note that in the case of List / Set, we need to define the type of the elements.
   * We therefore need a factory function that accepts a type parameter, and so use a def rather than a val.
   *
   * The unfortunate consequence of this, is that it requires the client to specify an explicit type parameter
   * when using makeListMonoid or makeSetMonoid in-line within combineAll.  For example:
   *
   *   combineAll(List(List(1), List(2, 3), List(4, 5, 6)), makeListMonoid[Int]) shouldBe List(1, 2, 3, 4, 5, 6)
   *
   * This can be eliminated by moving the typeClass into a separate argument list, which allows the type of A to
   * be captured from the type of the List of values.
   */
  def combineAll2[A](values: List[A])(typeClass: Monoid[A]): A =
    values.foldLeft(typeClass.empty)(typeClass.combine)


  /*
   * A number of syntactic changes can be made to the combineAll function to minimise the
   * intrusion of the typeClass argument.
   *
   * One convention is to give the typeClass argument the same name as the type parameter
   * (in this case A).  The typeClass is enriching what we know about the type, and this
   * syntax alludes to the fact that this information relates to the type.
   */
  def combineAll3[A](values: List[A])(A: Monoid[A]): A =
    values.foldLeft(A.empty)(A.combine)

  /*
   * It is also common to make the type class an implicit argument.
   */
  def combineAll4[A](values: List[A])(implicit A: Monoid[A]): A =
    values.foldLeft(A.empty)(A.combine)

  /*
   * Scala 2.8 introduced a shorthand for threading through implicit arguments, called 'context bounds'.
   * A type parameter with a context bound has the form [T: Bound], which is expanded to a type parameter T
   * and an implicit parameter of type Bound[T].
   *
   * The cost of this change to the function signature is that the body must now use implicitly to obtain a
   * reference to the typeClass.
   */
  def combineAll5[A: Monoid](values: List[A]): A = {
    val A = implicitly[Monoid[A]]
    values.foldLeft(A.empty)(A.combine)
  }

  /*
   * It is therefore common to provide a utility function in the companion object to the typeClass, that
   * can resolve the implicit reference for you.
   *
   *   object Monoid {
   *     def apply[A: Monoid]: Monoid[A] =
   *       implicitly[Monoid[A]]
   *   }
   *
   * Client code can then obtain a reference via this factory function as if it were referring to a
   * singleton object.
   */
  def combineAll6[A: Monoid](values: List[A]): A = {
    val A = Monoid[A]
    values.foldLeft(A.empty)(A.combine)
  }

  /*
   * This lookup is then typically in-lined.
   */
  def combineAll7[A: Monoid](values: List[A]): A =
    values.foldLeft(Monoid[A].empty)(Monoid[A].combine)
}