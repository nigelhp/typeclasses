package com.nigelhp

trait Monoid[A] {
  def empty: A
  def combine(x: A, y: A): A
}

object Monoid {
  def apply[A: Monoid]: Monoid[A] =
    implicitly[Monoid[A]]
}