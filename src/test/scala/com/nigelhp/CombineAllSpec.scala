package com.nigelhp

import com.nigelhp.CombineAll._
import org.scalatest.{FreeSpec, Matchers}

class CombineAllSpec extends FreeSpec with Matchers {
  "sumIntegers" - {
    "returns the identity value when the list is empty" in {
      sumIntegers(List.empty) shouldBe 0
    }

    "returns the lone value when the list contains a single integer" in {
      sumIntegers(List(42)) shouldBe 42
    }

    "returns the sum of all integers when the list contains multiple integers" in {
      sumIntegers(List(1, 2, 3, 4)) shouldBe 10
    }
  }

  "concatenateStrings" - {
    "returns the identity value when the list is empty" in {
      concatenateStrings(List.empty) shouldBe ""
    }

    "returns the lone value when the list contains a single string" in {
      concatenateStrings(List("hello")) shouldBe "hello"
    }

    "returns the concatenation of all strings when the list contains multiple strings" in {
      concatenateStrings(List("abra", "ca", "dab", "ra")) shouldBe "abracadabra"
    }
  }

  "joinLists" - {
    "returns the identity value when the list is empty" in {
      joinLists(List.empty) shouldBe Nil
    }

    "returns the lone value when the list contains a single list" in {
      joinLists(List(List(1, 2))) shouldBe List(1, 2)
    }

    "returns the concatenation of all constituent lists when the list contains multiple lists" in {
      joinLists(List(List(1), List(2, 3), List(4, 5, 6))) shouldBe List(1, 2, 3, 4, 5, 6)
    }
  }

  "unionSets" - {
    "returns the identity value when the list is empty" in {
      unionSets(List.empty) shouldBe Set.empty
    }

    "returns the lone value when the list contains a single set" in {
      unionSets(List(Set(1, 2))) shouldBe Set(1, 2)
    }

    "returns the union of all constituent sets when the list contains multiple sets" in {
      unionSets(List(Set(1), Set(2, 3), Set(3, 4, 5))) shouldBe Set(1, 2, 3, 4, 5)
    }
  }

  "combineAll" - {
    "can sum integers" - {
      "when the list is empty" in {
        combineAll(List.empty, intMonoid) shouldBe 0
      }

      "when the list contains a single integer" in {
        combineAll(List(42), intMonoid) shouldBe 42
      }

      "when the list contains multiple integers" in {
        combineAll(List(1, 2, 3, 4), intMonoid) shouldBe 10
      }
    }

    "can concatenate strings" - {
      "when the list is empty" in {
        combineAll(List.empty, stringMonoid) shouldBe ""
      }

      "when the list contains a single string" in {
        combineAll(List("hello"), stringMonoid) shouldBe "hello"
      }

      "when the list contains multiple strings" in {
        combineAll(List("abra", "ca", "dab", "ra"), stringMonoid) shouldBe "abracadabra"
      }
    }

    /*
     * Note that an explicit type parameter must be supplied to makeListMonoid / makeSetMonoid.
     */

    "can join lists" - {
      "when the list is empty" in {
        combineAll(List.empty, makeListMonoid[Int]) shouldBe Nil
      }

      "when the list contains a single list" in {
        combineAll(List(List(1, 2)), makeListMonoid[Int]) shouldBe List(1, 2)
      }

      "when the list contains multiple lists" in {
        combineAll(List(List(1), List(2, 3), List(4, 5, 6)), makeListMonoid[Int]) shouldBe List(1, 2, 3, 4, 5, 6)
      }
    }

    "can union sets" - {
      "when the list is empty" in {
        combineAll(List.empty, makeSetMonoid[Int]) shouldBe Set.empty
      }

      "when the list contains a single set" in {
        combineAll(List(Set(1, 2)), makeSetMonoid[Int]) shouldBe Set(1, 2)
      }

      "when the list contains multiple sets" in {
        combineAll(List(Set(1), Set(2, 3), Set(3, 4, 5)), makeSetMonoid[Int]) shouldBe Set(1, 2, 3, 4, 5)
      }
    }
  }

  "combineAll2 (non-implicit typeClass in separate argument list)" - {
    "can sum integers" - {
      "when the list is empty" in {
        combineAll2(List.empty)(intMonoid) shouldBe 0
      }

      "when the list contains a single integer" in {
        combineAll2(List(42))(intMonoid) shouldBe 42
      }

      "when the list contains multiple integers" in {
        combineAll2(List(1, 2, 3, 4))(intMonoid) shouldBe 10
      }
    }

    "can concatenate strings" - {
      "when the list is empty" in {
        combineAll2(List.empty)(stringMonoid) shouldBe ""
      }

      "when the list contains a single string" in {
        combineAll2(List("hello"))(stringMonoid) shouldBe "hello"
      }

      "when the list contains multiple strings" in {
        combineAll2(List("abra", "ca", "dab", "ra"))(stringMonoid) shouldBe "abracadabra"
      }
    }

    /*
     * Note that an explicit type parameter no longer needs to be supplied to makeListMonoid / makeSetMonoid.
     */

    "can join lists" - {
      "when the list is empty" in {
        combineAll2(List.empty)(makeListMonoid) shouldBe Nil
      }

      "when the list contains a single list" in {
        combineAll2(List(List(1, 2)))(makeListMonoid) shouldBe List(1, 2)
      }

      "when the list contains multiple lists" in {
        combineAll2(List(List(1), List(2, 3), List(4, 5, 6)))(makeListMonoid) shouldBe List(1, 2, 3, 4, 5, 6)
      }
    }

    "can union sets" - {
      "when the list is empty" in {
        combineAll2(List.empty)(makeSetMonoid) shouldBe Set.empty
      }

      "when the list contains a single set" in {
        combineAll2(List(Set(1, 2)))(makeSetMonoid) shouldBe Set(1, 2)
      }

      "when the list contains multiple sets" in {
        combineAll2(List(Set(1), Set(2, 3), Set(3, 4, 5)))(makeSetMonoid) shouldBe Set(1, 2, 3, 4, 5)
      }
    }
  }

  "combineAll4 (implicit typeClass parameter)" - {
    "can sum integers" - {
      implicit val typeClass = intMonoid

      "when the list is empty" in {
        combineAll4(List.empty) shouldBe 0
      }

      "when the list contains a single integer" in {
        combineAll4(List(42)) shouldBe 42
      }

      "when the list contains multiple integers" in {
        combineAll4(List(1, 2, 3, 4)) shouldBe 10
      }
    }

    "can concatenate strings" - {
      implicit val typeClass = stringMonoid

      "when the list is empty" in {
        combineAll4(List.empty) shouldBe ""
      }

      "when the list contains a single string" in {
        combineAll4(List("hello")) shouldBe "hello"
      }

      "when the list contains multiple strings" in {
        combineAll4(List("abra", "ca", "dab", "ra")) shouldBe "abracadabra"
      }
    }

    "can join lists" - {
      implicit val typeClass = makeListMonoid[Int]

      "when the list is empty" in {
        combineAll4(List.empty) shouldBe Nil
      }

      "when the list contains a single list" in {
        combineAll4(List(List(1, 2))) shouldBe List(1, 2)
      }

      "when the list contains multiple lists" in {
        combineAll4(List(List(1), List(2, 3), List(4, 5, 6))) shouldBe List(1, 2, 3, 4, 5, 6)
      }
    }

    "can union sets" - {
      implicit val typeClass = makeSetMonoid[Int]

      "when the list is empty" in {
        combineAll4(List.empty) shouldBe Set.empty
      }

      "when the list contains a single set" in {
        combineAll4(List(Set(1, 2))) shouldBe Set(1, 2)
      }

      "when the list contains multiple sets" in {
        combineAll4(List(Set(1), Set(2, 3), Set(3, 4, 5))) shouldBe Set(1, 2, 3, 4, 5)
      }
    }
  }

  "combineAll7 (function signature uses context bounds)" - {
    "can sum integers" - {
      implicit val typeClass = intMonoid

      "when the list is empty" in {
        combineAll7(List.empty) shouldBe 0
      }

      "when the list contains a single integer" in {
        combineAll7(List(42)) shouldBe 42
      }

      "when the list contains multiple integers" in {
        combineAll7(List(1, 2, 3, 4)) shouldBe 10
      }
    }

    "can concatenate strings" - {
      implicit val typeClass = stringMonoid

      "when the list is empty" in {
        combineAll7(List.empty) shouldBe ""
      }

      "when the list contains a single string" in {
        combineAll7(List("hello")) shouldBe "hello"
      }

      "when the list contains multiple strings" in {
        combineAll7(List("abra", "ca", "dab", "ra")) shouldBe "abracadabra"
      }
    }

    "can join lists" - {
      implicit val typeClass = makeListMonoid[Int]

      "when the list is empty" in {
        combineAll7(List.empty) shouldBe Nil
      }

      "when the list contains a single list" in {
        combineAll7(List(List(1, 2))) shouldBe List(1, 2)
      }

      "when the list contains multiple lists" in {
        combineAll7(List(List(1), List(2, 3), List(4, 5, 6))) shouldBe List(1, 2, 3, 4, 5, 6)
      }
    }

    "can union sets" - {
      implicit val typeClass = makeSetMonoid[Int]

      "when the list is empty" in {
        combineAll7(List.empty) shouldBe Set.empty
      }

      "when the list contains a single set" in {
        combineAll7(List(Set(1, 2))) shouldBe Set(1, 2)
      }

      "when the list contains multiple sets" in {
        combineAll7(List(Set(1), Set(2, 3), Set(3, 4, 5))) shouldBe Set(1, 2, 3, 4, 5)
      }
    }
  }
}